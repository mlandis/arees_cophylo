# git-based project template
---------------

Fork and rename to create new projects.


Contents
```
code   : analysis source code
data   : raw and formatted input files
docs   : cited manuscripts, technical articles, etc.
etc    : miscellaneous files
output : analysis output files
tex    : LaTeX manuscript source code
```